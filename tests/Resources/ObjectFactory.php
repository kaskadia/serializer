<?php

namespace Kaskadia\Lib\Serializer\Tests\Resources;

use Faker\Generator;
use Faker\Factory;
use Kaskadia\Lib\Serializer\Tests\Resources\Objects\GroupsAttributes;
use Kaskadia\Lib\Serializer\Tests\Resources\Objects\IgnoredAttributes;
use Kaskadia\Lib\Serializer\Tests\Resources\Objects\User;
use Kaskadia\Lib\Serializer\Tests\Resources\Objects\Company;
use Symfony\Component\Serializer\Annotation\Ignore;

class ObjectFactory {
	/** @var int */
	private int $times;
	/** @var Generator */
	private Generator $faker;

	public function __construct() {
		$this->faker = Factory::create();
	}

	public function times(int $times): self {
		$this->times = $times;
		return $this;
	}

	/**
	 * @param string $class
	 * @param array $attributes
	 * @return array
	 */
	public function make(string $class, array $attributes = []): array {
		$entities = [];
		for ($x = 0; $x < $this->times; $x++) {
			$entities[] = $this->makeOne($class, $attributes);
		}
		$this->times(1);
		return $entities;
	}

	/**
	 * @param string $class
	 * @param array $attributes
	 * @return Company|User|null
	 */
	public function makeOne(string $class, array $attributes = []) {
		switch ($class) {
			case User::class:
				return $this->getUser($attributes);
				break;
			case Company::class:
				return $this->getCompany($attributes);
				break;
			case GroupsAttributes::class:
				return $this->getGroupsObject();
				break;
			case IgnoredAttributes::class:
				return $this->getIgnoredObject();
				break;
		}
		return null;
	}

	private function getUser($attributes = []): User {
		$name = $this->getAttributeValue($attributes, 'name', $this->faker->name);
		$username = $this->getAttributeValue($attributes, 'username', $this->faker->email);
		$password = $this->getAttributeValue($attributes, 'password', $this->faker->password);
		$age = $this->getAttributeValue($attributes, 'age', $this->faker->numberBetween(10, 90));
		$birthDate = $this->getAttributeValue($attributes, 'birthDate', $this->faker->dateTimeBetween("-80 years", "now"));
		$randomDecimal = $this->getAttributeValue($attributes, 'randomDecimal', $this->faker->randomFloat());
		/** @var ?Company $company */
		$company = $this->getAttributeValue($attributes, 'company', null);
		$user = User::initialize($name, $username, $password, $age, $birthDate, $randomDecimal, $company);
		if (isset($company)) {
			$companyUser = $company->getUser();
			if (isset($companyUser)) {
				return $companyUser;
			}
			$company->setUser($user);
		} else {
			$company = $this->getCompany(['user' => $user]);
		}
		$user->setCompany($company);
		return $user;
	}

	private function getCompany($attributes = []): Company {
		$name = $this->getAttributeValue($attributes, 'name', $this->faker->company);
		$birthDate = $this->getAttributeValue($attributes, 'birthDate', $this->faker->dateTimeBetween("-15 years", 'now'));
		/** @var ?User $user */
		$user = $this->getAttributeValue($attributes, 'user', null);
		$company = Company::initialize($name, $birthDate, $user);
		if (isset($user)) {
			$userCompany = $user->getCompany();
			if (isset($userCompany)) {
				return $userCompany;
			}
			$user->setCompany($company);
		} else {
			$user = $this->getUser(['company' => $company]);
		}
		$company->setUser($user);
		return $company;
	}

	private function getIgnoredObject() {
		$name = $this->faker->name;
		$num = $this->faker->numberBetween();
		$field1 = $this->faker->catchPhrase;
		$field2 = $this->faker->catchPhrase;
		$alwaysIgnored = $this->faker->catchPhrase;
		return new IgnoredAttributes(
			$name,
			$num,
			$field1,
			$field2,
			$alwaysIgnored
		);
	}

	private function getGroupsObject() {
		$group1 = $this->faker->catchPhrase;
		$group2 = $this->faker->catchPhrase;
		$bothGroups = $this->faker->catchPhrase;
		$intGroup1 = $this->faker->numberBetween();
		$intGroup2 = $this->faker->numberBetween();
		$noGroups = $this->faker->macAddress;
		return new GroupsAttributes(
			$group1,
			$group2,
			$bothGroups,
			$intGroup1,
			$intGroup2,
			$noGroups
		);
	}

	private function getAttributeValue($attributes, $attributeName, $defaultValue) {
		return array_key_exists($attributeName, $attributes) ? $attributes[$attributeName] : $defaultValue;
	}
}
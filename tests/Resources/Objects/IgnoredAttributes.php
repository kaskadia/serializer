<?php

namespace Kaskadia\Lib\Serializer\Tests\Resources\Objects;

use Symfony\Component\Serializer\Annotation\Ignore;

class IgnoredAttributes {
	//<editor-fold desc="PROPERTIES" defaultstate="collapsed">
	public string $name;
	public int $num;
	public ?string $field1;
	public ?string $field2;
	// Test include default doctrine proxy field
	public static array $lazyPropertiesNames = array('foo');
	/**
	 * @Ignore()
	 */
	public ?string $alwaysIgnored;
	//</editor-fold>

	public function __construct(string $name, int $num, ?string $field1 = null, ?string $field2 = null, ?string $alwaysIgnored = null) {
		$this->name = $name;
		$this->num = $num;
		$this->field1 = $field1;
		$this->field2 = $field2;
		$this->alwaysIgnored = $alwaysIgnored;
	}

	//<editor-fold desc="SERIALIZATION" defaultState="collapsed">
	public const IGNORED_1 = [
		"field1"
	];

	public const IGNORED_2 = [
		"field2"
	];

	public const IGNORED_MERGE_DEFAULT = [
		"field1",
		"field2",
		"lazyPropertiesName"
	];
	//</editor-fold>
}
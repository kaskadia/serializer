<?php

namespace Kaskadia\Lib\Serializer\Tests\Resources\Objects;

use Symfony\Component\Serializer\Annotation\Groups;

class GroupsAttributes {
	//<editor-fold desc="PROPERTIES" defaultstate="collapsed">
	/**
	 * @Groups({"group1"})
	 */
	public ?string $group1;
	/**
	 * @Groups({"group2"})
	 */
	public ?string $group2;
	/**
	 * @Groups({"group1","group2"})
	 */
	public ?string $bothGroups;
	/**
	 * @Groups({"group1"})
	 */
	public ?int $intGroup1;
	/**
	 * @Groups({"group2"})
	 */
	public ?int $intGroup2;
	public ?string $noGroups;
	//</editor-fold>

	public function __construct(?string $group1 = null, ?string $group2 = null, ?string $bothGroups = null, ?int $intGroup1 = null, ?int $intGroup2 = null, ?string $noGroups = null) {
		$this->group1 = $group1;
		$this->group2 = $group2;
		$this->bothGroups = $bothGroups;
		$this->intGroup1 = $intGroup1;
		$this->intGroup2 = $intGroup2;
		$this->noGroups = $noGroups;
	}
}
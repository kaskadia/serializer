<?php

namespace Kaskadia\Lib\Serializer\Tests\Resources\Objects;

use DateTime;
use Symfony\Component\Serializer\Annotation\Ignore;

class User {
	//<editor-fold desc="PROPERTIES" defaultstate="collapsed">
	/** @var string */
	private string $name;
	/** @var string */
	private string $username;
	/**
	 * @var ?string
	 * @Ignore()
	 */
	private ?string $password;
	/** @var int */
	private int $age;
	/** @var DateTime */
	private DateTime $birthDate;
	/** @var float */
	private float $randomDecimal;
	/** @var ?Company */
	private ?Company $company;

	//</editor-fold>

	private function __construct(string $name, string $username, ?string $password, int $age, DateTime $birthDate, float $randomDecimal, ?Company $company) {
		$this->setName($name)
				->setUsername($username)
				->setPassword($password)
				->setAge($age)
				->setBirthDate($birthDate)
				->setRandomDecimal($randomDecimal)
				->setCompany($company);
	}

	public static function initialize(
			string $name = null,
			string $username = null,
			?string $password = null,
			int $age = null,
			DateTime $birthDate = null,
			float $randomDecimal = null,
			?Company $company = null): self {
		return new self($name, $username, $password, $age, $birthDate, $randomDecimal, $company);
	}

	//<editor-fold desc="ACCESSORS" defaultstate="collapsed">
	public function getName(): string {
		return $this->name;
	}

	public function setName(string $name): self {
		if (!isset($this->name)) {
			$this->name = $name;
		}
		return $this;
	}

	public function getUsername(): string {
		return $this->username;
	}

	public function setUsername(string $username): self {
		if (!isset($this->username)) {
			$this->username = $username;
		}
		return $this;
	}

	public function getPassword(): ?string {
		if (!isset($this->password)) {
			return null;
		}
		return $this->password;
	}

	public function setPassword(string $password): ?self {
		$this->password = $password;
		return $this;
	}

	public function getAge(): int {
		return $this->age;
	}

	public function setAge(int $age): self {
		if (!isset($this->age)) {
			$this->age = $age;
		}
		return $this;
	}

	public function getBirthDate(): DateTime {
		return $this->birthDate;
	}

	public function setBirthDate(DateTime $birthDate): self {
		if (!isset($this->birthDate)) {
			$this->birthDate = $birthDate;
		}
		return $this;
	}

	public function getRandomDecimal(): float {
		return $this->randomDecimal;
	}

	public function setRandomDecimal(float $randomDecimal): self {
		if (!isset($this->randomDecimal)) {
			$this->randomDecimal = $randomDecimal;
		}
		return $this;
	}

	public function getCompany(): ?Company {
		return $this->company;
	}

	public function setCompany(?Company $company): self {
		if (!isset($this->company)) {
			$this->company = $company;
		}
		return $this;
	}
	//</editor-fold>
}
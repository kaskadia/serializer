<?php

namespace Kaskadia\Lib\Serializer\Tests\Resources\Objects;

use DateTime;

class DecoratedCompany extends Company {
	protected function __construct(string $name, DateTime $createdDate, User $user) {
		parent::__construct($name, $createdDate, $user);
	}

	public static function initializeWithCompany(Company $company) {
		return new self($company->getName(), $company->getCreatedDate(), $company->getUser());
	}
}
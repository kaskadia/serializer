<?php

namespace Kaskadia\Lib\Serializer\Tests\Resources\Objects;

use DateTime;
use Symfony\Component\Serializer\Annotation\Ignore;

class Company {
	//<editor-fold desc="PROPERTIES" defaultstate="collapsed">
	/** @var string */
	private string $name;
	/** @var DateTime */
	private DateTime $createdDate;
	/**
	 * @var ?User
	 * @Ignore()
	 */
	private ?User $user;

	//</editor-fold>

	protected function __construct(string $name, DateTime $createdDate, ?User $user) {
		$this->setName($name)
				->setCreatedDate($createdDate)
				->setUser($user);
	}

	public static function initialize(string $name, DateTime $createdDate, ?User $user): self {
		return new self($name, $createdDate, $user);
	}

	//<editor-fold desc="ACCESSORS" defaultstate="collapsed">
	public function getName(): string {
		return $this->name;
	}

	public function setName(string $name): self {
		if (!isset($this->name)) {
			$this->name = $name;
		}
		return $this;
	}

	public function getCreatedDate(): DateTime {
		return $this->createdDate;
	}

	public function setCreatedDate(DateTime $createdDate): self {
		if (!isset($this->createdDate)) {
			$this->createdDate = $createdDate;
		}
		return $this;
	}

	public function getUser(): ?User {
		if (!isset($this->user)) {
			return null;
		}
		return $this->user;
	}

	public function setUser(?User $user): self {
		$this->user = $user;
		return $this;
	}
	//</editor-fold>
}
<?php

namespace Kaskadia\Lib\Serializer\Tests\Unit;

use Doctrine\Common\Collections\ArrayCollection;
use Kaskadia\Lib\Serializer\Serializer;
use Kaskadia\Lib\Serializer\Tests\Resources\Objects\{GroupsAttributes,
	IgnoredAttributes,
	User,
	Company,
	DecoratedCompany};
use Kaskadia\Lib\Serializer\Tests\Resources\ObjectFactory;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Tightenco\Collect\Support\Arr;
use Tightenco\Collect\Support\Collection;

class SerializerTest extends TestCase {
	/**
	 * @var ObjectFactory
	 */
	private ObjectFactory $factory;

	/**
	 * @var Serializer
	 */
	private Serializer $serializer;

	public function setUp(): void {
		$this->factory = new ObjectFactory();
		$this->serializer = Serializer::initialize();
	}

	public function testCanSerializeJsonAndDeserializeJson(): void {
		$user = $this->factory->makeOne(User::class);
		$json = $this->serializer->toJson($user);
		$deserializedUser = $this->serializer->fromJson($json, User::class);
		$this->assertDeserializedUserIsSame($user, $deserializedUser);
	}

	public function testCanSerializeDeserializeIlluminateCollection(): void {
		$users = $this->factory->times(10)->make(User::class);
		$collection = new Collection($users);
		$json = $this->serializer->toJson($collection);
		$deserializedArray = $this->serializer->fromJsonArray($json, User::class);
		$deserializedCollection = new Collection($deserializedArray);
		for ($x = 0; $x < $collection->count(); $x++) {
			$this->assertDeserializedUserIsSame($collection->get($x), $deserializedCollection->get($x));
		}
	}

	public function testCanSerializeDeserializeArrayCollection(): void {
		$users = $this->factory->times(10)->make(User::class);
		$collection = new ArrayCollection($users);
		$json = $this->serializer->toJson($collection);
		$deserializedArray = $this->serializer->fromJsonArray($json, User::class);
		$deserializedCollection = new ArrayCollection($deserializedArray);
		for ($x = 0; $x < $collection->count(); $x++) {
			$this->assertDeserializedUserIsSame($collection->get($x), $deserializedCollection->get($x));
		}
	}

	public function testCanSerializeDeserializeDecorators(): void {
		$company = $this->factory->makeOne(Company::class);
		$decoratedCompany = DecoratedCompany::initializeWithCompany($company);
		$json = $this->serializer->toJson($decoratedCompany);
		$deserializedDecoratedCompany = $this->serializer->fromJson($json, DecoratedCompany::class);
		$this->assertDeserializedCompanyIsSame($decoratedCompany, $deserializedDecoratedCompany);
	}

	public function testCanSerializeDeserializeEmptyArray(): void {
		$emptyArray = [];
		$json = $this->serializer->toJson($emptyArray);
		$deserializedArray = $this->serializer->fromJsonArray($json, Company::class);
		$this->assertEquals($emptyArray, $deserializedArray);
	}

	public function testCanSerializeDeserializeNullObject(): void {
		$nullObject = null;
		$json = $this->serializer->toJson($nullObject);
		$deserializedNullObject = $this->serializer->fromJson($json, Company::class);
		$this->assertEquals($nullObject, $deserializedNullObject);
	}

	public function testSerializeIgnoreAttributes(): void {
		$company = $this->factory->makeOne(Company::class);
		$user = $company->getUser();
		$this->assertEquals($company, $user->getCompany());
		$this->assertEquals($user, $user->getCompany()->getUser());
		$companyJson = $this->serializer->toJson($company);
		/** @var Company $deserializedCompany */
		$deserializedCompany = $this->serializer->fromJson($companyJson, Company::class);
		$this->assertDeserializedCompanyIsSame($company, $deserializedCompany);

		$user = $this->factory->makeOne(User::class);
		$company = $user->getCompany();
		$this->assertEquals($user, $company->getUser());
		$this->assertEquals($company, $company->getUser()->getCompany());
		$userJson = $this->serializer->toJson($user);
		/** @var User $deserializedUser */
		$deserializedUser = $this->serializer->fromJson($userJson, User::class);
		$this->assertDeserializedUserIsSame($user, $deserializedUser);
	}

	public function testSerializeIgnoredAttributesInArray(): void {
		$companies = $this->factory->times(3)->make(Company::class);
		$users = $this->factory->times(3)->make(User::class);
		$companiesJson = $this->serializer->toJson($companies);
		$usersJson = $this->serializer->toJson($users);
		$deserializedCompanies = $this->serializer->fromJsonArray($companiesJson, Company::class);
		$deserializedUsers = $this->serializer->fromJsonArray($usersJson, User::class);
		for ($x = 0; $x < count($companies); $x++) {
			$this->assertDeserializedCompanyIsSame($companies[$x], $deserializedCompanies[$x]);
		}
		for ($x = 0; $x < count($users); $x++) {
			$this->assertDeserializedUserIsSame($users[$x], $deserializedUsers[$x]);
		}
	}

	public function testSerializeIgnoredAttributesWithMerge(): void {
		$ignoredObject = $this->factory->makeOne(IgnoredAttributes::class);
		$ignored1Json = $this->serializer->toJson($ignoredObject, [AbstractNormalizer::IGNORED_ATTRIBUTES => IgnoredAttributes::IGNORED_1]);
		$ignored2Json = $this->serializer->toJson($ignoredObject, [AbstractNormalizer::IGNORED_ATTRIBUTES => IgnoredAttributes::IGNORED_2]);
		$ignoredMergeJson = $this->serializer->toJson($ignoredObject, [AbstractNormalizer::IGNORED_ATTRIBUTES => IgnoredAttributes::IGNORED_MERGE_DEFAULT]);
		$desIgnored1 = $this->serializer->fromJson($ignored1Json, IgnoredAttributes::class);
		$desIgnored2 = $this->serializer->fromJson($ignored2Json, IgnoredAttributes::class);
		$desIgnoredMerge = $this->serializer->fromJson($ignoredMergeJson, IgnoredAttributes::class);
		// Original Object
		$this->assertNotNull($ignoredObject->name);
		$this->assertNotNull($ignoredObject->num);
		$this->assertNotNull($ignoredObject->field1);
		$this->assertNotNull($ignoredObject->field2);
		$this->assertNotNull($ignoredObject->alwaysIgnored);
		$this->assertNotNull($ignoredObject::$lazyPropertiesNames);
		//Ignore 1
		$this->assertStringNotContainsStringIgnoringCase("lazyPropertiesNames", $ignored1Json);
		$this->assertNotNull($desIgnored1->name);
		$this->assertNotNull($desIgnored1->num);
		$this->assertNull($desIgnored1->field1);
		$this->assertNotNull($desIgnored1->field2);
		$this->assertNull($desIgnored1->alwaysIgnored);
		$this->assertNotNull($desIgnored1::$lazyPropertiesNames);
		//Ignore 2
		$this->assertStringNotContainsStringIgnoringCase("lazyPropertiesNames", $ignored2Json);
		$this->assertNotNull($desIgnored2->name);
		$this->assertNotNull($desIgnored2->num);
		$this->assertNotNull($desIgnored2->field1);
		$this->assertNull($desIgnored2->field2);
		$this->assertNull($desIgnored2->alwaysIgnored);
		$this->assertNotNull($desIgnored2::$lazyPropertiesNames);
		//Ignore Merge
		$this->assertStringNotContainsStringIgnoringCase("lazyPropertiesNames", $ignoredMergeJson);
		$this->assertNotNull($desIgnoredMerge->name);
		$this->assertNotNull($desIgnoredMerge->num);
		$this->assertNull($desIgnoredMerge->field1);
		$this->assertNull($desIgnoredMerge->field2);
		$this->assertNull($desIgnoredMerge->alwaysIgnored);
		$this->assertNotNull($desIgnoredMerge::$lazyPropertiesNames);
	}

	public function testSerializeGroupsAttributes(): void {
		$groupsObject = $this->factory->makeOne(GroupsAttributes::class);
		$group1Json = $this->serializer->toJson($groupsObject, [AbstractNormalizer::GROUPS => "group1"]);
		$group2Json = $this->serializer->toJson($groupsObject, [AbstractNormalizer::GROUPS => "group2"]);
		$groupBothJson = $this->serializer->toJson($groupsObject, [AbstractNormalizer::GROUPS => ["group1", "group2"]]);
		$noGroupsJson = $this->serializer->toJson($groupsObject);
		$desGroup1 = $this->serializer->fromJson($group1Json, GroupsAttributes::class);
		$desGroup2 = $this->serializer->fromJson($group2Json, GroupsAttributes::class);
		$desGroupBoth = $this->serializer->fromJson($groupBothJson, GroupsAttributes::class);
		$desNoGroups = $this->serializer->fromJson($noGroupsJson, GroupsAttributes::class);
		// Original Object
		$this->assertNotNull($groupsObject->group1);
		$this->assertNotNull($groupsObject->group2);
		$this->assertNotNull($groupsObject->bothGroups);
		$this->assertNotNull($groupsObject->intGroup1);
		$this->assertNotNull($groupsObject->intGroup2);
		$this->assertNotNull($groupsObject->noGroups);
		// Group 1
		$this->assertNotNull($desGroup1->group1);
		$this->assertNull($desGroup1->group2);
		$this->assertNotNull($desGroup1->bothGroups);
		$this->assertNotNull($desGroup1->intGroup1);
		$this->assertNull($desGroup1->intGroup2);
		$this->assertNull($desGroup1->noGroups);
		// Group 2
		$this->assertNull($desGroup2->group1);
		$this->assertNotNull($desGroup2->group2);
		$this->assertNotNull($desGroup2->bothGroups);
		$this->assertNull($desGroup2->intGroup1);
		$this->assertNotNull($desGroup2->intGroup2);
		$this->assertNull($desGroup2->noGroups);
		// Both Groups
		$this->assertNotNull($desGroupBoth->group1);
		$this->assertNotNull($desGroupBoth->group2);
		$this->assertNotNull($desGroupBoth->bothGroups);
		$this->assertNotNull($desGroupBoth->intGroup1);
		$this->assertNotNull($desGroupBoth->intGroup2);
		$this->assertNull($desGroupBoth->noGroups);
		// No Groups
		$this->assertNotNull($desNoGroups->group1);
		$this->assertNotNull($desNoGroups->group2);
		$this->assertNotNull($desNoGroups->bothGroups);
		$this->assertNotNull($desNoGroups->intGroup1);
		$this->assertNotNull($desNoGroups->intGroup2);
		$this->assertNotNull($desNoGroups->noGroups);
	}

	private function assertDeserializedUserIsSame(User $user, User $deserializedUser) {
		$this->assertInstanceOf(User::class, $deserializedUser);
		$this->assertEquals($user->getName(), $deserializedUser->getName());
		$this->assertEquals($user->getUsername(), $deserializedUser->getUsername());
		$this->assertEquals($user->getAge(), $deserializedUser->getAge());
		$this->assertEquals($user->getBirthDate(), $deserializedUser->getBirthdate());
		$this->assertEquals($user->getRandomDecimal(), $deserializedUser->getRandomDecimal());
		$this->assertNotNull($user->getPassword());
		$this->assertNull($deserializedUser->getPassword());
		$this->assertDeserializedCompanyIsSame($user->getCompany(), $deserializedUser->getCompany());
	}

	private function assertDeserializedCompanyIsSame(Company $company, Company $deserializedCompany) {
		$this->assertInstanceOf(Company::class, $deserializedCompany);
		$this->assertEquals($company->getName(), $deserializedCompany->getName());
		$this->assertEquals($company->getCreatedDate(), $deserializedCompany->getCreatedDate());
		$this->assertNotNull($company->getUser());
		$this->assertNull($deserializedCompany->getUser());
	}
}

<?php

namespace Kaskadia\Lib\Serializer;

use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\PropertyInfo\Extractor\PhpDocExtractor;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\PropertyInfo\PropertyInfoExtractor;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer as SymSerializer;

final class Serializer {
	/**
	 * @var SymSerializer
	 */
	private SymSerializer $serializer;
	private const JSON_TYPE = "json";
	private const XML_TYPE = "xml";
	private const IGNORED_CONTEXT_KEY = "ignored_attributes";

	// This default ignored attributes list is to ensure that Doctrine Proxies are able to be serialized without errors.
	// Some Doctrine Proxy classes get these added properties that don't play nicely with the serializer.
	protected const IGNORED_ATTRIBUTES = [
		'__cloner__',
		'__initializer__',
		'__isInitialized__',
		'lazyPropertiesNames',
		'lazyPropertiesDefaults'
	];

	private function __construct(SymSerializer $serializer) {
		$this->serializer = $serializer;
	}

	public static function initialize(): self {
		$encoders = [new XmlEncoder(), new JsonEncoder()];
		$classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
		$extractor = new PropertyInfoExtractor([], [new ReflectionExtractor(), new PhpDocExtractor()]);
		$normalizers = [
				new ArrayDenormalizer(),
				new DateTimeNormalizer(['datetime_format' => 'Y-m-d H:i:s.u T']),
				new ObjectNormalizer($classMetadataFactory, null, null, $extractor)
		];
		return new self(new SymSerializer($normalizers, $encoders));
	}

	private function deserialize(?string $data, string $type, string $format) {
		if (empty($data)) {
			return null;
		}
		return $this->serializer->deserialize($data, $type, $format);
	}

	private function serialize($data, string $format, array $context): ?string {
		if (empty($data)) {
			if (is_array($data)) {
				return "[]";
			}
			return null;
		}
		$defaultIgnored = [self::IGNORED_CONTEXT_KEY => self::IGNORED_ATTRIBUTES];
		if(!empty($context)) {
			if(array_key_exists(self::IGNORED_CONTEXT_KEY, $context)) {
				$context[self::IGNORED_CONTEXT_KEY] = array_merge($context[self::IGNORED_CONTEXT_KEY], $defaultIgnored[self::IGNORED_CONTEXT_KEY]);
			} else {
				$context = array_merge($context, $defaultIgnored);
			}
		} else {
			$context = $defaultIgnored;
		}
		return $this->serializer->serialize($data, $format, $context);
	}

	public function fromJson(?string $data, string $type) {
		return $this->deserialize($data, $type, self::JSON_TYPE);
	}

	public function fromJsonArray(string $data, string $type) {
		return $this->deserialize($data, $type . '[]', self::JSON_TYPE);
	}

	public function fromXml(string $data, string $type) {
		return $this->deserialize($data, $type, self::XML_TYPE);
	}

	public function fromXmlArray(string $data, string $type) {
		return $this->deserialize($data, $type . '[]', self::XML_TYPE);
	}

	public function toJson($data, $context = []): ?string {
		return $this->serialize($data, self::JSON_TYPE, $context);
	}

	public function toXml($data, $context = []): ?string {
		return $this->serialize($data, self::XML_TYPE, $context);
	}
}